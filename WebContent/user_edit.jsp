<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bulletin board</title>
	</head>
	<body>

		<h1>User Edit</h1>

		<p>Edit User ID : ${ editUserId }</p>

		<form action="user_edit" method="post">
			<label for="account">Account</label>
			<input type="text" id="account" name="account"><br/>

			<label for="password">Password</label>
			<input type="password" id="password" name="password"><br/>

			<label for="confirmation">Confirmation</label>
			<input type="password" id="confirmation" name="confirmation"><br/>

			<label for="name">Name</label>
			<input type="text" id="name" name="name"><br/>

			<label for="branch">Branch</label>
			<input type="text" id="branch" name="branch"><br/>

			<label for="department">Department</label>
			<input type="text" id="department" name="department"><br/>

			<button type="submit" name="userId" value="${ editUserId }">Update</button>
		</form>

		<a href="user_management">Back</a>
	</body>
</html>