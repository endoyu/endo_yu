<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bulletin board</title>
	</head>
	<body>

		<h1>Home</h1>

		<a href="home">Home</a>

		<ul>
			<li><a href="post_new">Post New</a></li>
			<li><a href="user_management">User Management</a></li>
		</ul>

		<a href="logout">Logout</a>

		<form action="home" method="post">
			<select name="display">
				<option value="" selected>Display Rule</option>
				<option value="registeredDate">Posted Date</option>
				<option value="category">Category</option>
			</select>

			<br/>

			<label for="fromDate">Display Period</label>
			<input type="text" id="fromDate" name="fromDate">
			~
			<input type="text" name="toDate">

			<br/>

			<label for="category">Category</label>
			<input type="text" id="category" name="category">

			<br/>

			<button type="submit">Change Display</button>
		</form>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<div class="posts">
			<c:forEach items="${ posts }" var="post">
				<div class="post">
					<div class="subject"><c:out value="${ post.subject }" /></div>
					<div class="text"><c:out value="${ post.text }" /></div>
					<div class="category"><c:out value="${ post.category }" /></div>
					<div class="date"><fmt:formatDate value="${ post.registeredDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					<div class="poster"><c:out value="${ post.poster }" /></div>
				</div>

				<c:if test="${ post.poster == loginUser.name }">
					<form action="home" method="post">
						<button type="submit" name="deletePost" value="delete">Delete</button>
					</form>
				</c:if>

				<div>
					<form action="home" method="post">
						<textarea id ="comment" name="text" cols="20" rows="2"></textarea>
						<button type="submit" name="postId" value="${ post.id }">Comment</button>
					</form>
				</div>

				<div>
					<c:forEach items="${ comments }" var="comment">
						<li>
						<c:out value="${ comment.text }" /><br/>
						<c:out value="${ comment.userId }" /><br/>
						<c:out value="${ comment.registeredDate }" />

						<c:if test="${ comment.userId == loginUser.id }">
							<form action="delete_comment" method="post">
								<button type="submit" name="deleteComment" value="${ comment.id }">Delete</button>
							</form>
						</c:if>
					</c:forEach>
				</div>
			</c:forEach>
		</div>

	</body>
</html>