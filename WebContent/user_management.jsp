<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bulletin board</title>
	</head>
	<body>

		<h1>User Management</h1>

		<a href="user_registration">User Registration</a><br/>

		<ul>
			<c:forEach items="${ users }" var="user">
				<li>
				<c:out value="${ user.id }" /><br/>
				<c:out value="${ user.account }" /><br/>
				<c:out value="${ user.password }" /><br/>
				<c:out value="${ user.name }" /><br/>
				<c:out value="${ user.branch }" /><br/>
				<c:out value="${ user.department }" /><br/>
				<c:out value="${ user.status }" /><br/>

				<form action="edit" method="post">
					<button type="submit" name="userId" value="${ user.id }">Edit</button>
				</form>

				<form action="user_management" method="post">
					<input type="submit" name="suspend" value="Suspend">
					<input type="submit" name="resume" value="Resume">
				</form>
			</c:forEach>
		</ul>

		<a href="home">Back</a>

	</body>
</html>