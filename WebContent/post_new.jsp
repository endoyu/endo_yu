<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html: charset=UTF-8">
		<title>Bulletin board</title>
	</head>
	<body>

		<h1>Post New</h1>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="post_new" method="post">
			<label for="subject">Subject</label>
			<input type="text" id="subject" name="subject"><br/>

			<label for="text">Text</label>
			<textarea id="text" name="text" cols="100" rows="5"></textarea><br/>

			<label for="category">Category</label>
			<input type="text" id="category" name="category"><br/>

			<input type="submit" value="Post"><br/>
		</form>

		<a href="home">Back</a>

	</body>
</html>