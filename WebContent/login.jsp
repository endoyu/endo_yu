<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bulletin board</title>
	</head>
	<body>

		<h1>Login</h1>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="login" method="post">
			<label for="account">Account</label>
			<input type="text" id="account" name="account"><br/>

			<label for="password">Password</label>
			<input type="password" id="password" name="password"><br/>

			<input type="submit" value="Login">
		</form>

	</body>
</html>