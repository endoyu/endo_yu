package endo_yu.beans;

import java.io.Serializable;
import java.util.Date;

public class Post implements Serializable {

	private int id;
	private String subject;
	private String text;
	private String category;
	private int userId;
	private Date registeredDate;
	private Date updatedDate;

	public void setId(int id)
	{
		this.id =id;
	}
	public int getId()
	{
		return this.id;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getSubject()
	{
		return this.subject;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	public String getText()
	{
		return this.text;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}
	public String getCategory()
	{
		return this.category;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public int getUserId()
	{
		return this.userId;
	}

	public void setRegisteredDate(Date registeredDate)
	{
		this.registeredDate = registeredDate;
	}
	public Date getRegisteredDate()
	{
		return this.registeredDate;
	}

	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}
	public Date getUpdatedDate()
	{
		return this.updatedDate;
	}
}
