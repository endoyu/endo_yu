package endo_yu.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

	public static final int RESUMED = 0;
	public static final int SUSPENDED = 1;

	private int id;
	private String account;
	private String password;
	private String name;
	private int branch;
	private int department;
	private int status;
	private Date registeredDate;
	private Date updatedDate;

	public void setId(int id)
	{
		this.id = id;
	}
	public int getId()
	{
		return this.id;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}
	public String getAccount()
	{
		return this.account;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return this.password;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}

	public void setBranch(int branch)
	{
		this.branch = branch;
	}
	public int getBranch()
	{
		return this.branch;
	}

	public void setDepartment(int department)
	{
		this.department = department;
	}
	public int getDepartment()
	{
		return this.department;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}
	public int getStatus()
	{
		return this.status;
	}

	public void setRegisteredDate(Date registeredDate)
	{
		this.registeredDate = registeredDate;
	}
	public Date getRegisteredDate()
	{
		return this.registeredDate;
	}

	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}
	public Date getUpdatedDate()
	{
		return this.updatedDate;
	}
}
