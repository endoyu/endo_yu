package endo_yu.beans;

import java.io.Serializable;
import java.util.Date;

public class ShownPost implements Serializable {

	private int id;
	private String subject;
	private String text;
	private String category;
	private Date registeredDate;
	private String poster;
//	private Comment comment;

	public void setId(int id)
	{
		this.id = id;
	}
	public int getId()
	{
		return this.id;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getSubject()
	{
		return this.subject;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	public String getText()
	{
		return this.text;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}
	public String getCategory()
	{
		return this.category;
	}

	public void setRegisteredDate(Date registeredDate)
	{
		this.registeredDate = registeredDate;
	}
	public Date getRegisteredDate()
	{
		return this.registeredDate;
	}

	public void setPoster(String poster)
	{
		this.poster = poster;
	}
	public String getPoster()
	{
		return this.poster;
	}

//	public void setComment(Comment comment)
//	{
//		this.comment = comment;
//	}
//	public Comment getComment()
//	{
//		return this.comment;
//	}
}
