package endo_yu.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

	private int id;
	private String text;
	private int userId;
	private int postId;
	private Date registeredDate;
	private Date updatedDate;

	public void setId(int id)
	{
		this.id = id;
	}
	public int getId()
	{
		return this.id;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	public String getText()
	{
		return this.text;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public int getUserId()
	{
		return this.userId;
	}

	public void setPostId(int postId)
	{
		this.postId = postId;
	}
	public int getPostId()
	{
		return this.postId;
	}

	public void setRegisteredDate(Date registeredDate)
	{
		this.registeredDate = registeredDate;
	}
	public Date getRegisteredDate()
	{
		return this.registeredDate;
	}

	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}
	public Date getUpdatedDate()
	{
		return this.updatedDate;
	}
}
