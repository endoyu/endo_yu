package endo_yu.dao;

import static endo_yu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import endo_yu.beans.Post;
import endo_yu.exception.SQLRuntimeException;

public class PostDao {

	public void insert(Connection connection, Post post)
	{
		PreparedStatement ps = null;

		try
		{
			StringBuilder sql = new StringBuilder();

			sql.append("insert into posts ( ");
			sql.append("subject, ");
			sql.append("text, ");
			sql.append("category, ");
			sql.append("user_id, ");
			sql.append("registered_date, ");
			sql.append("updated_date ");
			sql.append(") values ( ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("current_timestamp, ");
			sql.append("current_timestamp ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, post.getSubject());
			ps.setString(2, post.getText());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getUserId());

			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}
}
