package endo_yu.dao;

import static endo_yu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import endo_yu.beans.ShownPost;
import endo_yu.exception.SQLRuntimeException;

public class ShownPostDao {

	public List<ShownPost> select(Connection connection, int num)
	{
		PreparedStatement ps = null;

		try
		{
			StringBuilder sql = new StringBuilder();

			sql.append("select ");
			sql.append("posts.id as id, ");
			sql.append("posts.subject as subject, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("posts.registered_date as registered_date, ");
			sql.append("users.name as name ");
			sql.append("from posts ");
			sql.append("inner join users ");
			sql.append("on posts.user_id = users.id ");
			sql.append("order by registered_date desc limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<ShownPost> posts = toShownPosts(rs);
			return posts;
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	private List<ShownPost> toShownPosts(ResultSet rs) throws SQLException
	{
		List<ShownPost> posts = new ArrayList<>();

		try
		{
			while (rs.next())
			{
				ShownPost post = new ShownPost();

				post.setId(rs.getInt("id"));
				post.setSubject(rs.getString("subject"));
				post.setText(rs.getString("text"));
				post.setCategory(rs.getString("category"));
				post.setRegisteredDate(rs.getTimestamp("registered_date"));
				post.setPoster(rs.getString("name"));

				posts.add(post);
			}
			return posts;
		}
		finally
		{
			close(rs);
		}
	}
}
