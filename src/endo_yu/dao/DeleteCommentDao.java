package endo_yu.dao;

import static endo_yu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import endo_yu.exception.SQLRuntimeException;

public class DeleteCommentDao {

	public void delete(Connection connection, int commentId)
	{
		PreparedStatement ps = null;

		try
		{
			String sql = "delete from comments where id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, commentId);

			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}
}
