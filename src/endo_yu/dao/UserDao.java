package endo_yu.dao;

import static endo_yu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import endo_yu.beans.User;
import endo_yu.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user)
	{
		PreparedStatement ps = null;

		try
		{
			StringBuilder sql = new StringBuilder();

			sql.append("insert into users ( ");
			sql.append("account, ");
			sql.append("password, ");
			sql.append("name, ");
			sql.append("branch, ");
			sql.append("department, ");
			sql.append("status, ");
			sql.append("registered_date, ");
			sql.append("updated_date ");
			sql.append(") values ( ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("current_timestamp, ");
			sql.append("current_timestamp ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDepartment());
			ps.setInt(6, user.getStatus());

			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	public List<User> select(Connection connection)
	{
		PreparedStatement ps = null;

		try
		{
			String sql = "select * from users";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty())
			{
				return null;
			}

			return users;
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	public User select(Connection connection, String account)
	{
		PreparedStatement ps = null;

		try
		{
			String sql = "select * from users where account = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			if (!rs.next())
			{
				return null;
			}

			User user = toUser(rs);

			return user;
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	public User select(Connection connection, String account, String password)
	{
		PreparedStatement ps = null;

		try
		{
			String sql = "select * from users where (account = ?) and (password = ?)";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty())
			{
				return null;
			}
			else if (2 <= users.size())
			{
				throw new IllegalStateException("This user is duplicated.");
			}

			return users.get(0);
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	private User toUser(ResultSet rs) throws SQLException
	{
		User user = new User();

		try
		{
			user.setId(rs.getInt("id"));
			user.setAccount(rs.getString("account"));
			user.setPassword(rs.getString("password"));
			user.setName(rs.getString("name"));
			user.setBranch(rs.getInt("branch"));
			user.setDepartment(rs.getInt("department"));

			return user;
		}
		finally
		{
			close(rs);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLException
	{
		List<User> users = new ArrayList<>();

		try
		{
			while (rs.next())
			{
				User user = new User();

				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranch(rs.getInt("branch"));
				user.setDepartment(rs.getInt("department"));
				user.setStatus(rs.getInt("status"));
				user.setRegisteredDate(rs.getTimestamp("registered_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}

			return users;
		}
		finally
		{
			close(rs);
		}
	}
}
