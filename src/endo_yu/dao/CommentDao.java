package endo_yu.dao;

import static endo_yu.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import endo_yu.beans.Comment;
import endo_yu.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment)
	{
		PreparedStatement ps = null;

		try
		{
			StringBuilder sql = new StringBuilder();

			sql.append("insert into comments ( ");
			sql.append("text, ");
			sql.append("user_id, ");
			sql.append("post_id, ");
			sql.append("registered_date, ");
			sql.append("updated_date ");
			sql.append(") values ( ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("?, ");
			sql.append("current_timestamp, ");
			sql.append("current_timestamp ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());

			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	public List<Comment> select(Connection connection)
	{
		PreparedStatement ps = null;

		try
		{
			StringBuilder sql = new StringBuilder();

			sql.append("select ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as id, ");
			sql.append("comments.registered_date as registered_date ");
			sql.append("from comments ");
			sql.append("inner join posts ");
			sql.append("on comments.post_id = posts.id ");
			sql.append("order by registered_date desc");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Comment> comments = toComments(rs);
			return comments;
		}
		catch (SQLException e)
		{
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps);
		}
	}

	private List<Comment> toComments(ResultSet rs) throws SQLException
	{
		List<Comment> comments = new ArrayList<>();

		try
		{
			while (rs.next())
			{
				Comment comment = new Comment();

				comment.setText(rs.getString("text"));
				comment.setUserId(rs.getInt("id"));
				comment.setRegisteredDate(rs.getDate("registered_date"));

				comments.add(comment);
			}
			return comments;
		}
		finally
		{
			close(rs);
		}
	}
}
