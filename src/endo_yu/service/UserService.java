package endo_yu.service;

import static endo_yu.utils.CloseableUtil.*;
import static endo_yu.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import endo_yu.beans.User;
import endo_yu.dao.UserDao;
import endo_yu.utils.CipherUtil;

public class UserService {

	public void insert(User user)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			new UserDao().insert(connection, user);

			commit(connection);
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}

	public User select(String account)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			User user = new UserDao().select(connection, account);

			commit(connection);
			return user;
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}

	public User select(String account, String password)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();
			String encPassword = CipherUtil.encrypt(password);

			User user = new UserDao().select(connection, account, encPassword);

			commit(connection);
			return user;
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}

	public List<User> select()
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			List<User> users = new UserDao().select(connection);

			commit(connection);
			return users;
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}

	public void update(HttpServletRequest request)
	{
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int department = Integer.parseInt(request.getParameter("department"));

		Connection connection = null;

		try
		{
			connection = getConnection();

			new UserDao().update(request, connection);

			commit(connection);
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}
}
