package endo_yu.service;

import static endo_yu.utils.CloseableUtil.*;
import static endo_yu.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import endo_yu.beans.Comment;
import endo_yu.dao.CommentDao;

public class CommentService {

	public void insert(Comment comment)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			new CommentDao().insert(connection, comment);

			commit(connection);
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}

	public List<Comment> select()
	{
		Connection connection = null;

		try
		{
			connection = getConnection();
			List<Comment> comments = new CommentDao().select(connection);
			commit(connection);

			return comments;
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}
}
