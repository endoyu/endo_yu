package endo_yu.service;

import static endo_yu.utils.CloseableUtil.*;
import static endo_yu.utils.DBUtil.*;

import java.sql.Connection;

import endo_yu.dao.DeleteCommentDao;

public class DeleteCommentService {

	public void delete(int commentId)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			new DeleteCommentDao().delete(connection, commentId);;

			commit(connection);
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}
}
