package endo_yu.service;

import static endo_yu.utils.CloseableUtil.*;
import static endo_yu.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import endo_yu.beans.Post;
import endo_yu.beans.ShownPost;
import endo_yu.dao.PostDao;
import endo_yu.dao.ShownPostDao;

public class PostService {

	public void insert(Post post)
	{
		Connection connection = null;

		try
		{
			connection = getConnection();

			new PostDao().insert(connection, post);

			commit(connection);
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}

	public List<ShownPost> select()
	{
		final int LIMIT_NUM = 1000;

		Connection connection = null;

		try
		{
			connection = getConnection();
			List<ShownPost> posts = new ShownPostDao().select(connection, LIMIT_NUM);
			commit(connection);

			return posts;
		}
		catch (RuntimeException e)
		{
			rollback(connection);
			throw e;
		}
		catch (Error e)
		{
			rollback(connection);
			throw e;
		}
		finally
		{
			close(connection);
		}
	}
}
