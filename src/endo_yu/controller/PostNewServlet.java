package endo_yu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import endo_yu.beans.Post;
import endo_yu.beans.User;
import endo_yu.service.PostService;

@WebServlet(urlPatterns = { "/post_new" })
public class PostNewServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		request.getRequestDispatcher("post_new.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Post post = getPost(request);
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(post, errorMessages))
		{
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("post_new.jsp").forward(request, response);
			return;
		}

		new PostService().insert(post);
		response.sendRedirect("/endo_yu/home");
	}


	private Post getPost(HttpServletRequest request) throws ServletException, IOException
	{
		Post post = new Post();
		User loginUser = (User) request.getSession().getAttribute("loginUser");

		post.setSubject(request.getParameter("subject"));
		post.setText(request.getParameter("text"));
		post.setCategory(request.getParameter("category"));
		post.setUserId(loginUser.getId());

		return post;
	}

	private boolean isValid(Post post, List<String> errorMessages)
	{
		String subject = post.getSubject();
		String text = post.getText();
		String category = post.getCategory();

		if (StringUtils.isBlank(subject))
		{
			errorMessages.add("Enter Subject.");
		}
		else if (30 < subject.length())
		{
			errorMessages.add("Enter Subject in 30 characters.");
		}

		if (StringUtils.isBlank(text))
		{
			errorMessages.add("Enter Text.");
		}
		else if (1000 < text.length())
		{
			errorMessages.add("Enter Text in 1000 characters.");
		}

		if (StringUtils.isBlank(category))
		{
			errorMessages.add("Enter Category.");
		}
		else if (10 < category.length())
		{
			errorMessages.add("Enter category in 10 characters.");
		}

		if (!errorMessages.isEmpty())
		{
			return false;
		}

		return true;
	}
}
