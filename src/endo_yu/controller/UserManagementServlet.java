package endo_yu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import endo_yu.beans.User;
import endo_yu.service.UserService;

@WebServlet(urlPatterns = { "/user_management" })
public class UserManagementServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(user))
		{
			errorMessages.add("You can't visit 'User Management' page.");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("/endo_yu/home");
			return;
		}

		List<User> users = new UserService().select();

		request.setAttribute("users", users);
		request.getRequestDispatcher("user_management.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

	}

	private boolean isValid(User user)
	{
		int branch = user.getBranch();
		int department = user.getDepartment();

		if (!(branch == 1 && department == 1))
		{
			return false;
		}

		return true;
	}
}
