package endo_yu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import endo_yu.beans.Comment;
import endo_yu.beans.ShownPost;
import endo_yu.beans.User;
import endo_yu.service.CommentService;
import endo_yu.service.PostService;

@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		List<ShownPost> posts = new PostService().select();
		List<Comment> comments = new CommentService().select();

		User loginUser = (User) request.getSession().getAttribute("loginUser");

		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("loginUser", loginUser);
		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Comment comment = getComment(request);
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(comment, errorMessages))
		{
			List<ShownPost> posts = new PostService().select();
			List<Comment> comments = new CommentService().select();

			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("posts", posts);
			request.setAttribute("comments", comments);
			request.getRequestDispatcher("home.jsp").forward(request, response);
			return;
		}

		new CommentService().insert(comment);
		response.sendRedirect("/endo_yu/home");
	}

	private Comment getComment(HttpServletRequest request) throws ServletException, IOException
	{
		Comment comment = new Comment();
		User user = (User) request.getSession().getAttribute("loginUser");
		int postId = Integer.parseInt(request.getParameter("postId"));

		comment.setText(request.getParameter("text"));
		comment.setUserId(user.getId());
		comment.setPostId(postId);

		return comment;
	}

	private boolean isValid(Comment comment, List<String> errorMessages)
	{
		String text = comment.getText();

		if (StringUtils.isBlank(text))
		{
			errorMessages.add("Enter Comment.");
		}
		else if (500 < text.length())
		{
			errorMessages.add("Enter Comment in 500 characters.");
		}

		if (!errorMessages.isEmpty())
		{
			return false;
		}

		return true;
	}
}
