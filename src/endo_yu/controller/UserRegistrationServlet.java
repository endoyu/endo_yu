package endo_yu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import endo_yu.beans.User;
import endo_yu.service.UserService;

@WebServlet(urlPatterns = { "/user_registration" })
public class UserRegistrationServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(user))
		{
			errorMessages.add("You can't visit 'User Registration' page.");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("/endo_yu/home");
			return;
		}

		request.getRequestDispatcher("user_registration.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		User user = getUser(request);
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(request, user, errorMessages))
		{
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("user_registration.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("/endo_yu/user_management");
	}

	private boolean isValid(User user)
	{
		int branch = user.getBranch();
		int department = user.getDepartment();

		if (!(branch == 1 && department == 1))
		{
			return false;
		}

		return true;
	}

	private User getUser(HttpServletRequest request) throws ServletException, IOException
	{
		User user = new User();

		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setDepartment(Integer.parseInt(request.getParameter("department")));
		user.setStatus(User.RESUMED);

		return user;
	}

	private boolean isValid(HttpServletRequest request, User user, List<String> errorMessages)
	{
		String account = user.getAccount();
		String password = user.getPassword();
		String confirmation = request.getParameter("confirmation");
		String name = user.getName();
//		int branch = user.getBranch();
//		int department = user.getDepartment();

		User registeredUser = new UserService().select(account);

		if (StringUtils.isBlank(account))
		{
			errorMessages.add("Enter Account.");
		}
		else if (account.length() < 6 || 20 < account.length())
		{
			errorMessages.add("Enter Account in 6 to 20 characters.");
		}
		else if (!account.matches("^[a-zA-Z0-9]{6,20}$"))
		{
			errorMessages.add("Enter Account using half-width characters.");
		}
		else if (account.equals(registeredUser.getAccount()))
		{
			errorMessages.add("This Account is already registered.");
		}

		if (StringUtils.isBlank(password))
		{
			errorMessages.add("Enter Password.");
		}
		else if (password.matches("^.{6,20}$"))
		{
			errorMessages.add("Enter Password in 20 characters.");
		}
		else if (!password.equals(confirmation))
		{
			errorMessages.add("Password can't be confirmed.");
		}

		if (10 < name.length())
		{
			errorMessages.add("Enter Name in 10 characters.");
		}

		if (!errorMessages.isEmpty())
		{
			return false;
		}

		return true;
	}
}
