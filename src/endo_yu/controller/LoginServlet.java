package endo_yu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import endo_yu.beans.User;
import endo_yu.service.UserService;

@WebServlet(urlPatterns = { "/index.jsp", "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String account = request.getParameter("account");
		String password = request.getParameter("password");

		User user = new UserService().select(account, password);
		List<String> errorMessages = new ArrayList<>();

		if (user == null)
		{
			errorMessages.add("Failed to login.");
		}
		  else if (user.getStatus() == User.SUSPENDED)
		{
			errorMessages.add("This account can't login.");
		}

		if (errorMessages.size() != 0)
		{
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("/login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("/endo_yu/home");
	}
}
